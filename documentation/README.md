# 1/4 Updates

"What the client wants now is a branching mechanism: IF you choose one new answer on the first question, THEN we want to take you on a different journey that skips a couple of the old questions and adds a new question."

## How it works

data.js has all the questions

index.js is the bulk of the app

view the project locally from build/index.html 

`npm run build` throws errors in (my) Windows, but `npm run build-js` works at least

## index.js Details

`GetData` is pulling items out of data.js

**Page#Content componenets**
Rendered inside of App  
These play the videos on the page  
They're given a show class    
And if the current_page has that page's number, the page component is shown  
So... all the page content is being rendered on the page at once and hidden with CSS...

**Aside#Content componenets**
There's one for each page  
Every Aside has 2 buttons  
* One to go forward and one backward  
* They pass a value into HandlePageChange   
* And changes which page content (and aside?) is shown   
* The next button is disabled until one of the Aside's options are selected  
```javascript
<button className="back" onClick={(e)=>{this.props.HandlePageChange(2)}}>Back</button>
<button disabled={this.props.data["3"]?false:true} onClick={(e)=>{return this.props.data["3"]?this.props.HandlePageChange(4):this.HandleError();}}><span>Next</span></button>
```

**App state.data**
`data` seems to be keeping track of the answers  
I assume it's used in some final calculation for the results? Maybe in `CompileStateData`  
`3-value` gets the number from the sliding scale on the 3rd page

**What makes something a page?**
I guess there's just a component for each page. There are 5 pages of questions, then a 6th page of results, ResultPageContent.

**What exactly is `handleData` doing?**
It's activaded onChange. So probably adjusting `data` values?

**And prducts are?...**
Product recommendations on the results page. Seems like somehow they're being checked against the app's state data...

![prduct results in data.js](./img/results.png)