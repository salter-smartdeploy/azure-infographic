export default function GetData(data_name) {
  const data = {
    why_azure: {
      reduce_time: {
        criteria: "a",
        header: "Reduce time to market",
        content: "Azure lets your team develop apps and analyze data instead of managing infrastructure. With no infrastructure to manage, your team can simply innovate using powerful data and artificial-intelligence (AI) services, along with built-in monitoring, threat detection, automatic patching, and backups to develop, test, and deploy solutions more quickly."
      },
      archive_high: {
        criteria: "b",
        header: "Achieve high availability and business continuity",
        content: "Each Azure region includes at least three Availability Zones. Each zone comprises one or more datacenters equipped with independent power, cooling, and networking. Applications and data are replicated across Availability Zones to protect them from single points of failure. You get the industry’s best availability, with an SLA of 99.99 percent virtual-machine (VM) uptime, and you can easily co-locate compute, network, and storage across Availability Zones.",
        link: "Learn more",
        link_url: "https://docs.microsoft.com/en-us/azure/availability-zones/az-overview"
      },
      scale_at_will: {
        criteria: "c",
        header: "Scale at will",
        content: "Azure includes virtually bottomless, low-cost cloud storage. You can extend storage without code changes to your existing applications, and Azure keeps your data encrypted for improved security. You can also migrate apps to the cloud anytime—even incrementally—with zero downtime. Azure is also portable across geographies, with availability in 140 countries.",
        link: "Learn more",
        link_url: "https://azure.microsoft.com/en-us/global-infrastructure/"
      },
      use_any_tool: {
        criteria: "d",
        header: "Use any tool, language, or framework",
        content: "Azure lets you build applications with your preferred tools and language—SQL, NoSQL, PostgreSQL, Apache Spark, Android, Apache Hadoop, Apache Hive, iOS, JavaScript Object Notation (JSON), Java, LLAP, .NET, Node, PHP, Python, Ruby, PowerShell, BASH, and REST APIs."
      },
      innovate: {
        criteria: "e",
        header: "Innovate instead of managing infrastructure",
        content: "With Azure, you aren’t managing infrastructure—Microsoft is. You simply do what you do best: analyze, develop, and power your company’s digital transformation. And if you need on-premises capabilities, Azure gives you the only hybrid cloud that is consistent across your on-premises datacenter and the cloud.",
        link: "Learn more",
        link_url: "https://azure.microsoft.com/en-us/overview/hybrid-cloud/"
      },
      security: {
        criteria: "f",
        header: "Maintain security and privacy",
        content: "Microsoft leads the industry in establishing and meeting clear security and privacy requirements, including General Data Protection Regulation (GDPR), ISO 27001, HIPAA, FedRAMP, SOC 1 and SOC 2, as well as country-specific standards, including Australia IRAP, UK G-Cloud, and Singapore MTCS. Built-in security controls and intelligence also help you easily identify and respond to threats and security gaps.",
        link: "Learn more",
        link_url: "https://azure.microsoft.com/en-us/overview/trusted-cloud/"
      }//,
      // geographic_reach: {
      //   criteria: "-",
      //   header: "Geographic reach",
      //   content: "Azure is portable across geographies, with availability in 140 countries.",
      //   link: "Learn more",
      //   link_url: "https://azure.microsoft.com/en-us/global-infrastructure/"
      // }
    },
    products: {
      "Azure SQL Database": {
        criteria: {
          "1a 3a 5a":true,
          "1a 3a 5b":true,
          "1a 3c 5a":true,
          "1a 3c 5b":1
        },
        benefits: [
          "Develop new applications or migrate existing applications to SQL Database Managed Instance without changing your apps and using the tools you know.",
          "Migrate Microsoft SQL Server databases without changing existing apps.",
          "Benefit from built-in intelligence that learns database patterns and adapts to maximize performance, reliability, and data protection.",
          "Access built-in protection and security features that dynamically mask sensitive data and encrypt it—at rest and in motion.",
          "Achieve 99.99 percent availability and recovery-point objectives (RPOs) of less than five seconds.",
          "Scale on the fly with minimal or no downtime.",
          "Maximize resource utilization and manage thousands of databases as one, while still ensuring one customer per database with elastic pools."
        ],
        link: "Get further insight on Azure SQL Database",
        link_url: "https://azure.microsoft.com/en-us/services/sql-database/",
        description: "Azure SQL Database is a fully managed relational cloud database service that lets you:",
        order: {
          dev: [1,2,3,4,5,6,7],
          dba: [7,1,4,5,6,2,3],
          scientist: [6,7,1,2,5,3,4],
          manager: [6,7,3,2,1,4,5],
          other: [1,2,3,4,5,6,7]
        },
        toggles: [
          {
            header: "See deployment options",
            always_show: true,
            content: [
              {
                header: "Single",
                content: "A database-scoped deployment option that provides predictable workload performance for apps requiring guaranteed resources at the database level."
              },
              {
                header: "Elastic pool",
                content: "A shared-resource model optimized for efficiency of multi-tenant applications. This option offers better cost efficiency for software-as-a-service (SaaS) apps with multiple databases that can share resources at the database level. You can mix pools with single databases."
              },
              {
                header: "Managed instance",
                content: "An instance-scoped deployment option that has high compatibility with SQL Server and full platform-as-a-service (PaaS) benefits to enable modernizing at scale with low friction and effort."
              }
            ]
          },
          {
            header: "Need to manage unstructured/non-relational data alongside your structured/relational data?",
            content: "Azure SQL Database lets you parse and query data represented in JavaScript Object Notation (JSON) format and export that relational data as JSON text. And, if you have a web service that takes data from the database layer and provides a response in JSON format or client-side JavaScript frameworks or libraries that accept data formatted as JSON, you can format your database content as JSON directly in a SQL query.",
            link: "Learn more",
            link_url: "https://docs.microsoft.com/en-us/azure/sql-database/sql-database-json-features",
            criteria: ["3c"]
          }
        ]
      },
      "Azure Database for PostgreSQL": {
        criteria: {
          "1a 3a 5b": true,
          "1a 3a 5c": true,
          "1a 3b 5b": true,
          "1a 3c 5b": 3,
          "1a 3c 5c": 3
        },
        benefits: [
          "Develop and deploy apps.",
          "Easily lift and shift apps to the cloud using the languages and frameworks you know.",
          "Access built-in high availability.",
          "Scale in seconds, so you can easily adjust to changing customer demands.",
          "Benefit from unparalleled security and compliance, including Azure IP Advantage.",
          "Get a flexible pay-as-you-go pricing model with no hidden costs."
        ],
        link: "Analyze Azure Database for PostgreSQL further",
        link_url: "https://azure.microsoft.com/en-us/services/postgresql/",
        description: "Azure Database for PostgreSQL is a fully managed, enterprise-ready community PostgreSQL database as a service that lets you:",
        order: {
          dev: [1,2,3,4,5,6],
          dba: [6,5,3,1,2,4],
          scientist: [6,4,3,2,1,5],
          manager: [6,5,1,3,4,2],
          other: [1,2,3,4,5,6]
        },
        toggles: []
      },
      "Azure Cosmos DB": {
        criteria: {
          "1a 3b 5a":true,
          "1a 3b 5b":true,
          "1a 3b 5c":true,
          "1a 3c 5a":true,
          "1a 3c 5b":4,
          "1a 3c 5c":1
        },
        benefits: [
          "Easily build and deploy applications in multiple geographies without managing datacenter configurations.",
          "Achieve low latency and 99.999 percent availability.",
          "Access native support for multiple data-storage paradigms—key/value store, graph, column-family, and document data, in addition to APIs for accessing data in SQL, JavaScript, Gremlin, MongoDB, Apache Cassandra, and Azure Table Storage.",
          "Access limitless scalability for storage and throughput while paying only for the storage you need.",
          "Choose from five consistency levels—strong, bonded staleness, consistent-prefix, session, and eventual."
        ],
        link: "Extend your knowledge of Azure Cosmos DB",
        link_url: "https://azure.microsoft.com/en-us/services/cosmos-db/",
        description: "Azure Cosmos DB is a comprehensive, fully managed, enterprise-grade database service that lets you:",
        order: {
          dev: [1,2,3,4,5],
          dba: [5,3,1,4,2],
          scientist: [4,1,2,3,5],
          manager: [3,1,5,2,4],
          other: [1,2,3,4,5]
        },
        toggles: [
          {
            header: "Need to manage structured/relational data alongside your unstructured/non-relational data?",
            content:"Azure Cosmos DB offers a SQL API that supports querying documents using SQL language and supports relational, hierarchical, and spatial queries.",
            link: "Learn more",
            link_url: "https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-introduction",
            criteria: ["3c"]
          }
        ]
      },
      "Azure Database for MySQL": {
        criteria: {
          "1a 3a 5b":true,
          "1a 3a 5c":true,
          "1a 3c 5b":2,
          "1a 3c 5c":2
        },
        benefits: [
          "Develop and deploy apps.",
          "Easily lift and shift apps to the cloud using the languages and frameworks you know.",
          "Access built-in high availability.",
          "Scale in seconds, so you can easily adjust to changing customer demands.",
          "Benefit from unparalleled security and compliance, including Azure IP Advantage.",
          "Get a flexible pay-as-you-go pricing model with no hidden costs."
        ],
        link: "Analyze Azure Database for MySQL further",
        link_url: "https://azure.microsoft.com/en-us/services/mysql/",
        description: "Azure Database for MySQL is a fully managed, enterprise-ready community MySQL database as a service that lets you:",
        order: {
          dev: [1,2,3,4,5,6],
          dba: [6,5,3,1,2,4],
          scientist: [6,5,2,3,1,4],
          manager: [6,5,1,4,2,3],
          other: [1,2,3,4,5,6]
        },
        toggles: []
      },
      "Azure Storage": {
        criteria: {
          "1b 3b 5a":true,
          "1b 3b 5b":true,
          "1b 3b 5c":true,
          "1b 3c 5a":true,
          "1b 3c 5b":true,
          "1b 3c 5c":true
        },
        benefits: [
          "Run massively parallel analytics with a solution built to the open Apache Hadoop Distributed File System (HDFS) standard.",
          "Store blobs, files, messaging, disk, archival, structured, semi-structured, and unstructured data with no limits on data size or volume.",
          "Access high 99.9 percent availability with built-in redundancy and added options to further replicate data across datacenters and geographies.",
          "Built-in encryption and fine-grained control over who can access data.",
          "Achieve massive scalability for throughput to support any size of analytic workload.",
          "Access data using the languages and frameworks you are used to.",
          "Easily migrate your existing Apache Hadoop and Apache Spark data to the cloud without recreating your HDFS directory structure."
        ],
        link: "Analyze Azure Storage further",
        link_url: "https://azure.microsoft.com/en-us/services/storage/",
        description: "Azure Storage is a fully managed cloud-storage solution for modern data-storage scenarios that lets you:",
        order: {
          dev: [1,2,3,4,5,6,7],
          dba: [7,4,6,1,5,3,2],
          scientist: [2,6,4,3,1,5,7],
          manager: [5,4,1,2,3,6,7],
          other: [1,2,3,4,5,6,7]
        },
        toggles: []
      },
      "Azure SQL Data Warehouse": {
        criteria: {
          "1b 3a 5a":true,
          "1b 3a 5b":true,
          "1b 3a 5c":true,
          "1b 3c 5a":true,
          "1b 3c 5b":true,
          "1b 3c 5c":true,
          "1c 3a 5a":true,
          "1c 3a 5b":true,
          "1c 3c 5a":true,
          "1c 3c 5b":true
        },
        benefits: [
          "Run 128 concurrent queries and store an unlimited amount of columnar data to run the largest and most complex analytics workloads.",
          "Independently scale compute and storage, while pausing and resuming your data warehouse within minutes.",
          "Create a hub for analytics with native connectivity for data-integration and visualization services while using your existing SQL and business intelligence (BI) skills.",
          "Access guaranteed high 99.9 percent availability, regulatory compliance, built-in advanced security features, and data sovereignty across more than 30 regions.",
          "Benefit from intelligent caching to accelerate data access and query performance to handle the most demanding data warehousing workloads.",
          "Access tight integration with Azure Databricks."
        ],
        link: "Get further insight on Azure SQL Data Warehouse",
        link_url: "https://azure.microsoft.com/en-us/services/sql-data-warehouse/",
        description: "Azure SQL Data Warehouse is a fully managed, high-performance, security-enabled cloud data warehouse that lets you:",
        order: {
          dev: [1,2,3,4,5,6],
          dba: [2,4,3,5,1,6],
          scientist: [1,6,2,4,3,5],
          manager: [2,5,3,1,4,6],
          other: [1,2,3,4,5,6]
        },
        toggles: [
          {
            header: "Need to manage unstructured/non-relational data alongside your structured/relational data?",
            content: "Use PolyBase to access, run queries on, or export data to an HDFS-compatible storage solution using T-SQL.",
            link: "Learn more",
            link_url: "https://docs.microsoft.com/en-us/sql/relational-databases/polybase/polybase-guide?view=sql-server-2017",
            criteria: [
              "3c",
              "3a 5a",
              "3a 5b"
            ]
          }
        ]
      },
      "Azure Databricks": {
        criteria: {
          "1c 3a 5a":true,
          "1c 3a 5b":true,
          "1c 3a 5c":true,
          "1c 3b 5a":true,
          "1c 3b 5b":true,
          "1c 3b 5c":true,
          "1c 3c 5a":true,
          "1c 3c 5b":true,
          "1c 3c 5c":true
        },
        benefits: [
          "Drive innovation and increase productivity with an interactive, collaborative workspace.",
          "Launch a new Spark environment with a single click.",
          "Effortlessly integrate with a variety of data stores and services.",
          "Add advanced artificial intelligence (AI) and machine learning capabilities instantly and share insights through integration with Power BI.",
          "Access a secure cloud with role-based controls, fine-grained user permissions, and enterprise-grade service-level agreements (SLAs).",
          "Scale analytics and data-science projects without limits and add capacity instantly.",
          "Access a complete set of analytics technologies including SQL, streaming, MLlib, and GraphX."
        ],
        link: "Analyze Azure Databricks further",
        link_url: "https://azure.microsoft.com/en-us/services/databricks/",
        description: "Azure Databricks is a fully managed, easy-to-use, high-performance, Apache Spark engine–based analytics platform that lets you:",
        order: {
          dev: [1,2,3,4,5,6,7],
          dba: [3,4,2,7,1,5,6],
          scientist: [5,7,6,1,4,2,3],
          manager: [1,7,5,4,2,3,6],
          other: [1,2,3,4,5,6,7]
        },
        toggles: [
          {
            header: "Need to manage structured/relational data alongside your unstructured/non-relational data?",
            content: "Use the Java Database Connectivity (JDBC) drivers that come with Azure Databricks to query Microsoft SQL Server and Azure SQL Database tables.",
            link: "Learn more",
            link_url: "https://docs.azuredatabricks.net/spark/latest/data-sources/sql-databases.html",
            criteria: ["3c","3a"]
          }
        ]
      },
      "Azure HDInsight": {
        criteria: {
          "1b 3a 5a":true,
          "1b 3a 5b":true,
          "1b 3a 5c":true,
          "1b 3b 5a":true,
          "1b 3b 5b":true,
          "1b 3b 5c":true,
          "1b 3c 5a":true,
          "1b 3c 5b":true,
          "1b 3c 5c":true,
          "1c 3a 5a":true,
          "1c 3a 5b":true,
          "1c 3a 5c":true,
          "1c 3b 5a":true,
          "1c 3b 5b":true,
          "1c 3b 5c":true,
          "1c 3c 5a":true,
          "1c 3c 5b":true,
          "1c 3c 5c":true
        },
        benefits: [
          "Quickly and cost-effectively process massive amounts of data.",
          "Use popular open-source frameworks for a broad range of scenarios. Frameworks include Apache Hadoop, Apache Spark, Apache Hive, LLAP, Apache Kafka, Apache Storm, R, and more. Scenarios include  extract, transform, and load (ETL), data warehousing, machine learning, the Internet of Things (IoT), and more.",
          "Get an end-to-end service-level agreement (SLA) for production workloads in geographies around the globe.",
          "Access enterprise-grade protection with monitoring, virtual networks, encryption, role-based access, and more, in addition to compatibility with industry and government compliance standards.",
          "Access machine learning capabilities instantly and obtain valuable insight from large amounts (petabytes, or even exabytes) of structured, unstructured, and fast-moving data using Spark ML, MLlib, R, Hive, and the Microsoft Cognitive Toolkit.",
          "Use the tools and languages you know, such as Microsoft Visual Studio, Eclipse, IntelliJ IDEA, Jupyter, Apache Zeppelin, Scala, Python, R, Java, and .NET.",
          "Seamlessly and instantly integrate with popular third-party big-data solutions.",
          "Create clusters on demand and scale up or down as needed.",
          "Get a flexible pay-only-for-what-you-use pricing model."
        ],
        link: "Extend your knowledge of HDInsights",
        link_url: "https://azure.microsoft.com/en-us/services/hdinsight/",
        description: "Azure HDInsight is a full-spectrum, open-source enterprise analytics service that lets you:",
        order: {
          dev: [1,2,3,4,5,6,7,8,9],
          dba: [4,5,3,1,7,7,8,6,9],
          scientist: [3,2,5,4,1,7,6,8,9],
          manager: [2,8,1,3,6,9,7,5,4],
          other: [1,2,3,4,5,6,7,8,9]
        },
        toggles: []
      }
    },
    roles: {
      "a": "manager",
      "b": "scientist",
      "c": "dba",
      "d": "dev",
      "e": "other"
    },
    questions: {
      "1": {
        "a": "Develop a new app or modernize an existing app",
        "a-hover": "App hosting in Azure",
        "b": "Add or modernize a big data or data warehouse solution",
        "b-hover": "Store large amounts of structured or unstructured data to mine for insights.",
        "c": "Add new analytics efforts or modernize existing analytics efforts",
        "c-hover": "Connect data to a high-performance analytics platform."
      },
      "2": {
        "a": "Reduce time to market",
        "a-hover": "Develop software more quickly and efficiently.",
        "b": "Achieve high availability and business continuity",
        "b-hover": "Prevent outages through data redundancy.",
        "c": "Scale at will",
        "c-hover": "Quickly adapt to storage needs that can grow rapidly and without limits.",
        "d": "Use any tool, language, or framework",
        "d-hover": "Provide a modern and flexible coding environment for developers.",
        "e": "Innovate instead of managing infrastructure",
        "e-hover": "Choose a fully managed service.",
        "f": "Maintain security and privacy",
        "f-hover": "Encrypt sensitive data at rest and in motion."
      },
      "3": {
        "a": "Structured/relational",
        "a-hover": "This is the format of data found in traditional databases, such as SQL databases.",
        "b": "Unstructured/non-relational",
        "b-hover": "All other data, including text files, word processing documents, email, multimedia, and web pages.",
        "c": "A mix"
      },
      "4": {
        "a": "IT or business manager/decision maker",
        "b": "Data scientist",
        "c": "Database administrator (DBA)",
        "d": "Developer",
        "e": "Other"
      },
      "5": {
        "c": "Yes",
        "a": "No",
        "b": "No preference"
      }
    }
  };

  return data[data_name];
};
