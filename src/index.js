import React from 'react';
import ReactDOM from 'react-dom';
// import {
//   LoadImage
// } from './utilities.js';
import './index.css';
import GetData from './data/data.js';
import logo_png from './img/test-logo.png';
import icon_rocket from './img/question1-icon-01.png';
import icon_building from './img/building-icon(2).png';
import icon_database from './img/db-icon(3).png';
import icon_admin from './img/admin-icon(4).png';
import icon_computer from './img/computer-icon(5).png';
// import tower_png from './img/tower.png';

import video_1 from './video/1_StackAnimation.mp4';
import video_2 from './video/2_StackAnimation.mp4';
import video_3 from './video/3_StackAnimation.mp4';
import video_4 from './video/4_StackAnimation.mp4';
import video_5 from './video/5_StackAnimation.mp4';

var modals = {};

function AddModal (id, modal) {
  modals[id] = modal;
}

// function OpenModal (id) {
//   modals[id].OpenModal();
// }

function ToggleModal (id) {
  modals[id].ToggleModal();
}

// function CloseModal (id) {
//   modals[id].CloseModal();
// }

class DropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      shown:false,
      finished:false,
      target:{scrollHeight:1000}
    };
  }

  CalcDuration = function (target) {
    return (Math.sqrt(target.scrollHeight) / 30);
  }

  HandleHeaderClick = function (e) {
    this.setState({
      shown: !this.state.shown,
      target: e.target.nextSibling,
      max_height: e.target.nextSibling.scrollHeight + "px"
    });
    if (!this.state.shown) {
      const _this = this;
      setTimeout(()=>{
        _this.setState({
          max_height: "none"
        });
      }, this.CalcDuration(e.target.nextSibling) * 1000);
    } else {
      requestAnimationFrame(()=>{
        this.setState({
          max_height: "0"
        });
      });
    }
  }

  render () {
    const target = this.state.target;
    return (
      <div className={this.props.className + " dropdown" + (this.state.shown?" show":"")}>
        <h3 onClick={(e)=>{this.HandleHeaderClick(e)}}>{this.props.header}</h3>
        <div style={{
          "maxHeight": this.state.max_height,
          "transitionDuration": this.CalcDuration(target) + "s"
        }}>
          {this.props.content}
        </div>
      </div>
    );
  }
}

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      shown:false
    };
  }

  ToggleModal = function () {
    if (this.state.shown) {
      this.CloseModal();
    } else {
      this.OpenModal();
    }
  }
  OpenModal = function () {
    this.setState({
      shown: true
    });
  }
  CloseModal = function () {
    this.setState({
      shown: false
    });
  }

  componentDidMount () {
    AddModal(this.props.id, {
      OpenModal: ()=>{this.OpenModal()},
      CloseModal: ()=>{this.CloseModal()},
      ToggleModal: ()=>{this.ToggleModal()}
    });
  }

  HandleHeaderClick = function () {
    this.CloseModal();
  }

  render () {
    return (
      <div className={this.props.className + " modal" + (this.state.shown?" show":"")}>
        <div>
          <header onClick={(e)=>{this.HandleHeaderClick(e)}} className="flex-container space-between">
            <h2>{this.props.header}</h2>
            <span className="close-modal">X</span>
          </header>
          <div>
            {this.props.content}
          </div>
        </div>
      </div>
    );
  }
}

class Aside1Content extends React.Component {
  HandleData = function (option) {
    this.props.HandleData({
      "1":option.target.value
    });
  }

  HandleError = function () {

  }

  render () {
    const answers = GetData("questions")["1"];
    return (
      <div id="aside-1" className={this.props.shown?"show":""}>
        <header>
          <p className="right">1/5</p>
          <div className="flex-container center">
            <img src={icon_rocket} alt="" />
            <div className="">
              <h2>What do you want to achieve?</h2>
              <h3>(Select one.)</h3>
            </div>
          </div>
          <span className="line-horizontal bottom"></span>
        </header>
        <form>
          <input onChange={(e)=>{this.HandleData(e)}} id="1a" value="a" type="radio" name="want-to-do" />
          <label htmlFor="1a">
            <span>
              <span>
              {answers["a"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["a-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="1b" value="b" type="radio" name="want-to-do" />
          <label htmlFor="1b">
            <span>
              <span>
              {answers["b"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["b-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="1c" value="c" type="radio" name="want-to-do" />
          <label htmlFor="1c">
            <span>
              <span>
              {answers["c"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["c-hover"]}
              </span>
            </div>
          </label>
        </form>
        <footer className="right">
          <button className="back" onClick={(e)=>{this.props.HandlePageChange(0)}}>Back</button>
          <button disabled={this.props.data["1"]?false:true} onClick={(e)=>{return this.props.data["1"]?this.props.HandlePageChange(2):this.HandleError();}}><span>Next</span></button>
        </footer>
      </div>
    );
  }
}

class Page1Content extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      played:false
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown && prevProps.shown) {
      setTimeout(()=>{
        document.getElementById("video-1").currentTime = 0;
        this.setState({
          played:false
        });
      }, 1000);
    }
  }


  render () {
    // if (this.props.shown && !this.state.shown) {
    //   setTimeout(()=>{
    //     this.setState({shown:true});
    //     document.getElementById("video-1").play();
    //   }, 1000);
    // }
    if (this.props.shown && !this.state.played) {
      setTimeout(()=>{
        requestAnimationFrame(()=>{
          this.setState({played:true});
          document.getElementById("video-1").play();
        });
      }, 1500);
    }



    return (
      <div id="page-1" className={this.props.shown?"show":""}>
        <video id="video-1">
          <source id="mp4_src" src={video_1} type="video/mp4" />
        </video>
      </div>
    );
  }
}


class Aside2Content extends React.Component {
  HandleData = function (option) {
    let value = "";
    const letters = [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f"
    ];
    const values = [
      document.getElementById("2a").checked,
      document.getElementById("2b").checked,
      document.getElementById("2c").checked,
      document.getElementById("2d").checked,
      document.getElementById("2e").checked,
      document.getElementById("2f").checked
    ];

    values.forEach((checked,index)=>{
      if (checked) {
        value+=letters[index];
      }
    });
    console.log(value);
    this.props.HandleData({
      "2":value
    });
  }

  HandleError = function () {

  }

  render () {
    const answers = GetData("questions")["2"];
    return (
      <div id="aside-2" className={this.props.shown?"show":""}>
        <header>
          <p className="right">2/5</p>
          <div className="flex-container center">
            <img src={icon_building} alt="" />
            <div className="">
              <h2>What is your most important business goal?</h2>
              <h3>(Select all that apply.)</h3>
            </div>
          </div>
          <span className="line-horizontal bottom"></span>
        </header>
        <form>
          <input onChange={(e)=>{this.HandleData(e)}} id="2a" type="checkbox" name="business-goal" />
          <label htmlFor="2a">
            <span>
              <span>
              {answers["a"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["a-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="2b" type="checkbox" name="business-goal" />
          <label htmlFor="2b">
            <span>
              <span>
              {answers["b"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["b-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="2c" type="checkbox" name="business-goal" />
          <label htmlFor="2c">
            <span>
              <span>
              {answers["c"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["c-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="2d" type="checkbox" name="business-goal" />
          <label htmlFor="2d">
            <span>
              <span>
              {answers["d"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["d-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="2e" type="checkbox" name="business-goal" />
          <label htmlFor="2e">
            <span>
              <span>
              {answers["e"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["e-hover"]}
              </span>
            </div>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="2f" type="checkbox" name="business-goal" />
          <label htmlFor="2f">
            <span>
              <span>
              {answers["f"]}
              </span>
            </span>
            <div className="label-hover">
              <span>
                {answers["f-hover"]}
              </span>
            </div>
          </label>
        </form>
        <footer className="right">
          <button className="back" onClick={(e)=>{this.props.HandlePageChange(1)}}>Back</button>
          <button disabled={this.props.data["2"]?false:true} onClick={(e)=>{return this.props.data["2"]?this.props.HandlePageChange(3):this.HandleError();}}><span>Next</span></button>
        </footer>
      </div>
    );
  }
}

class Page2Content extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      played:false
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown && prevProps.shown) {
      setTimeout(()=>{
        document.getElementById("video-2").currentTime = 0;
        this.setState({
          played:false
        });
      }, 1000);
    }
  }


  render () {
    // if (this.props.shown && !this.state.shown) {
    //   setTimeout(()=>{
    //     this.setState({shown:true});
    //     document.getElementById("video-2").play();
    //   }, 1000);
    // }
    if (this.props.shown && !this.state.played) {
      requestAnimationFrame(()=>{
        this.setState({played:true});
        document.getElementById("video-2").play();
      });
    }


    return (
      <div id="page-2" className={this.props.shown?"show":""}>
        <video id="video-2">
          <source id="mp4_src" src={video_2} type="video/mp4" />
        </video>
      </div>
    );
  }
}


class Aside3Content extends React.Component {
  HandleData = function (option) {
    const value = option.target.value < 33 ? "a" : option.target.value > 66 ? "b" : "c";
    this.props.HandleData({
      "3":value,
      "3-value":option.target.value
    });
  }

  HandleError = function () {

  }

  render () {
    // const answers = GetData("questions")["3"];
    return (
      <div id="aside-3" className={this.props.shown?"show":""}>
        <header>
          <p className="right">3/5</p>
          <div className="flex-container center">
            <img src={icon_database} alt="" />
            <div className="">
              <h2>Select the type of data do you have?</h2>
              <h3>(Position the slider over your data type/mix.)</h3>
            </div>
          </div>
          <span className="line-horizontal bottom"></span>
        </header>
        <div className="range-container">
          <input onChange={(e)=>{this.HandleData(e)}} type="range" id="start" name="volume" min="0" max="100" />
          <p className="flex-container space-between">
            <span>Structured/<br />relational</span>
            <span>A mix</span>
            <span>Unstructured/<br />non-relational</span>
          </p>
        </div>
        <footer className="right">
          <button className="back" onClick={(e)=>{this.props.HandlePageChange(2)}}>Back</button>
          <button disabled={this.props.data["3"]?false:true} onClick={(e)=>{return this.props.data["3"]?this.props.HandlePageChange(4):this.HandleError();}}><span>Next</span></button>
        </footer>
      </div>
    );
  }
}

class Page3Content extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      played:false
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown && prevProps.shown) {
      setTimeout(()=>{
        document.getElementById("video-3").currentTime = 0;
        this.setState({
          played:false
        });
      }, 1000);
    }
  }


  render () {
    // if (this.props.shown && !this.state.shown) {
    //   setTimeout(()=>{
    //     this.setState({shown:true});
    //     document.getElementById("video-3").play();
    //   }, 1000);
    // }
    if (this.props.shown && !this.state.played) {
      requestAnimationFrame(()=>{
        this.setState({played:true});
        document.getElementById("video-3").play();
      });
    }


    return (
      <div id="page-3" className={this.props.shown?"show":""}>
        <video id="video-3">
          <source id="mp4_src" src={video_3} type="video/mp4" />
        </video>
      </div>
    );
  }
}


class Aside4Content extends React.Component {
  HandleData = function (option) {
    this.props.HandleData({
      "4":option.target.value
    });
  }

  HandleError = function () {

  }

  render () {
    const roles = GetData("questions")["4"];
    return (
      <div id="aside-4" className={this.props.shown?"show":""}>
        <header>
          <p className="right">4/5</p>
          <div className="flex-container center">
            <img src={icon_admin} alt="" />
            <div className="">
              <h2>What best describes your role?</h2>
              <h3>(Select one.)</h3>
            </div>
          </div>
          <span className="line-horizontal bottom"></span>
        </header>
        <form>
          <input onChange={(e)=>{this.HandleData(e)}} id="4a" value="a" type="radio" name="role" />
          <label htmlFor="4a">
            <span>
              <span>
              {roles["a"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="4b" value="b" type="radio" name="role" />
          <label htmlFor="4b">
            <span>
              <span>
              {roles["b"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="4c" value="c" type="radio" name="role" />
          <label htmlFor="4c">
            <span>
              <span>
              {roles["c"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="4d" value="d" type="radio" name="role" />
          <label htmlFor="4d">
            <span>
              <span>
              {roles["d"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="4e" value="e" type="radio" name="role" />
          <label htmlFor="4e">
            <span>
              <span>
              {roles["e"]}
              </span>
            </span>
          </label>
        </form>
        <footer className="right">
          <button className="back" onClick={(e)=>{this.props.HandlePageChange(3)}}>Back</button>
          <button disabled={this.props.data["4"]?false:true} onClick={(e)=>{return this.props.data["4"]?this.props.HandlePageChange(5):this.HandleError();}}><span>Next</span></button>
        </footer>
      </div>
    );
  }
}

class Page4Content extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      played:false
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown && prevProps.shown) {
      setTimeout(()=>{
        document.getElementById("video-4").currentTime = 0;
        this.setState({
          played:false
        });
      }, 1000);
    }
  }


  render () {
    // if (this.props.shown && !this.state.shown) {
    //   setTimeout(()=>{
    //     this.setState({shown:true});
    //     document.getElementById("video-4").play();
    //   }, 1000);
    // }
    if (this.props.shown && !this.state.played) {
      requestAnimationFrame(()=>{
        this.setState({played:true});
        document.getElementById("video-4").play();
      });
    }


    return (
      <div id="page-4" className={this.props.shown?"show":""}>
        <video id="video-4">
          <source id="mp4_src" src={video_4} type="video/mp4" />
        </video>
      </div>
    );
  }
}

class Aside5Content extends React.Component {
  HandleData = function (option) {
    this.props.HandleData({
      "5":option.target.value
    });
  }

  HandleError = function () {

  }

  render () {
    const answers = GetData("questions")["5"];
    return (
      <div id="aside-5" className={this.props.shown?"show":""}>
        <header>
          <p className="right">5/5</p>
          <div className="flex-container center">
            <img src={icon_computer} alt="" />
            <div className="">
              <h2>Do you prefer open-source technologies?</h2>
              <h3>(Select one.)</h3>
            </div>
          </div>
          <span className="line-horizontal bottom"></span>
        </header>
        <form>
          <input onChange={(e)=>{this.HandleData(e)}} id="5a" value="a" type="radio" name="open-source" />
          <label htmlFor="5a">
            <span>
              <span>
              {answers["a"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="5b" value="b" type="radio" name="open-source" />
          <label htmlFor="5b">
            <span>
              <span>
              {answers["b"]}
              </span>
            </span>
          </label>
          <input onChange={(e)=>{this.HandleData(e)}} id="5c" value="c" type="radio" name="open-source" />
          <label htmlFor="5c">
            <span>
              <span>
              {answers["c"]}
              </span>
            </span>
          </label>
        </form>
        <footer className="right">
          <button className="back" onClick={(e)=>{this.props.HandlePageChange(4)}}>Back</button>
          <button disabled={this.props.data["5"]?false:true} onClick={(e)=>{return this.props.data["5"]?this.props.HandlePageChange(6):this.HandleError();}}><span>Get insight</span></button>
        </footer>
      </div>
    );
  }
}

class Page5Content extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      played:false
    };
  }

  componentDidUpdate(prevProps) {
    if (!this.props.shown && prevProps.shown) {
      setTimeout(()=>{
        document.getElementById("video-5").currentTime = 0;
        this.setState({
          played:false
        });
      }, 1000);
    }
  }


  render () {
    // if (this.props.shown && !this.state.shown) {
    //   setTimeout(()=>{
    //     this.setState({shown:true});
    //     document.getElementById("video-5").play();
    //   }, 1000);
    // }
    if (this.props.shown && !this.state.played) {
      requestAnimationFrame(()=>{
        this.setState({played:true});
        document.getElementById("video-5").play();
      });
    }



    return (
      <div id="page-5" className={this.props.shown?"show":""}>
        <video id="video-5">
          <source id="mp4_src" src={video_5} type="video/mp4" />
        </video>
      </div>
    );
  }
}

class ResultAsideContent extends React.Component {
  render () {
    const top_3_dropdowns = [];
    const data = GetData("why_azure");

    const keys = Object.keys(data);

    if (this.props.data["2"]) {
      keys.forEach((key, index) => {
        if (this.props.data["2"].indexOf(data[key].criteria)+1) {
          const link = [];
          if (data[key].link) {
            link.push((
              <a key="link" href={data[key].link_url} target="_new"> {data[key].link}</a>
            ));
            link.push((
              <span key="dot">.</span>
            ));
          }
          top_3_dropdowns.push((
            <DropDown
              header={data[key].header}
              content={(<p>{data[key].content}{link}</p>)}
              key={key}
            />
          ));
        }
      });
    }
    const answers = GetData("questions");

    const goal_answers = [];

    if (this.props.data["2"]) {
      const goals = this.props.data["2"].split("");
      goals.forEach((option,index)=>{
        goal_answers.push((
          <span key={option}>{answers["2"][option]}</span>
        ));
        if (index+1 < goals.length) {
          goal_answers.push((<span key={option + "-gap"} className="v-gap" />));
        }
      });
    }

    let data_type = "";

    if (this.props.data["3-value"] <= 25) {
      data_type = "Structured/relational";
    }
    else if (this.props.data["3-value"] > 25 && this.props.data["3-value"] <= 40) {
      data_type = "Mostly structured/relational";
    }
    else if (this.props.data["3-value"] > 40 && this.props.data["3-value"] <= 59) {
      data_type = "A mix of structured/unstructured";
    }
    else if (this.props.data["3-value"] > 60 && this.props.data["3-value"] <= 74) {
      data_type = "Mostly unstructured/non-relational";
    }
    else if (this.props.data["3-value"] > 74 && this.props.data["3-value"] <= 100) {
      data_type = "Unstructured/non-relational";
    }

// * At the 0 to 25% mark – show “Structured/relational”
// * At the 75 to 100% mark, show “Unstructured/non-relational”
// * At the 41 to 59% mark, show “A mix of structured/unstructured”
// * From 26% to 40%, show “Mostly structured/relational”
// * From 60 to 74%, show “Mostly unstructured/non-relational”

    return (
      <div id="aside-6" className={this.props.shown?"show":""}>
        <section>
          <p>
            <strong>What do you want to do?</strong><br />
            <span>{answers["1"][this.props.data["1"]]}</span>
          </p>
          <figure></figure>
          <p>
            <strong>What is your most important business goal?</strong><br />
            {goal_answers}
          </p>
          <p>
            <strong>What type of data do you have?</strong><br />
            <span>{data_type}</span>
          </p>
          <p>
            <strong>What best describes your role?</strong><br />
            <span>{answers["4"][this.props.data["4"]]}</span>
          </p>
          <p>
            <strong>Do you prefer open-source technologies?</strong><br />
            <span>{answers["5"][this.props.data["5"]]}</span>
          </p>
        </section>
        <section className="right">
          <button onClick={(e)=>{this.props.HandlePageChange(1)}}><span>Change criteria</span></button>
        </section>
        <span className="line-horizontal thin"></span>
        <section>
          <h2>Why Microsoft Azure?</h2>
          {top_3_dropdowns}
          <h3 className="a" onClick={(e)=>{ToggleModal("benefit-modal")}}>See all benefits</h3>
        </section>
      </div>
    );
  }
}

class ResultPageContent extends React.Component {

  GenerateDropDownContent (product_name, product_judge_string) {
    // const answers = GetData("questions");
    const products = GetData("products");

    const chosen_role = GetData("roles")[this.props.data["4"]];
    const product = products[product_name];
    const benefit_content = [];
    const toggles = [];
    const main_content = [];

    if (product) {
      product.benefits.forEach((benefit, b)=>{

        const order = product.order[chosen_role] ? (product.order[chosen_role][b] - 1) : b;
        benefit_content[order] = (
          <li key={chosen_role+"-"+b}>{benefit}</li>
        );
      });

      product.toggles.forEach((toggle, index) => {
        let meets_criteria = false;

        if (toggle.criteria) {
          toggle.criteria.forEach((criteria, index) => {
            if (product_judge_string.indexOf(criteria) > -1) {
              meets_criteria = true;
            }
          });
        }
        else {
          meets_criteria = true;
        }

        if (meets_criteria) {
          const content = [];
          const link = [];
          if (typeof toggle.content === "object") {
            toggle.content.forEach((content_object, index2) => {
              content.push((
                <div key={index2}>
                  <h4>{content_object.header}</h4>
                  <p>{content_object.content}</p>
                </div>
              ));
            });
          } else {
            if (toggle.link) {
              link.push((
                <a key="link" href={toggle.link_url} target="_new"> {toggle.link}</a>
              ));
              link.push((
                <span key="span">.</span>
              ));
            }
            content.push((
              <p key="text">{toggle.content}{link}</p>
            ));
          }

          if (!toggle.header) {
            toggles.push((
              <div key={index}>
                <p>{content}</p>
              </div>
            ));
          } else {
            toggles.push((
              <DropDown
                className="strong"
                key={index}
                header={toggle.header}
                content={content}
              />
            ));
          }
        }

      });

      main_content.push((
        <div key="product_content">
          <p>{product.description}</p>
          <ul>{benefit_content}</ul>
          {toggles}
          <div>
            <a className="button" href={product.link_url} target="_new">{product.link}</a>
          </div>
        </div>
      ));
    }
    return main_content;
  }

  GenerateModalContent () {
    const data = GetData("why_azure");
    const keys = Object.keys(data);
    const other_benefits = [];
    keys.forEach((key, index) => {
      const link = [];
      if (data[key].link) {
        link.push((
          <a key="link" href={data[key].link_url} target="_new"> {data[key].link}</a>
        ));
        link.push((
          <span key="dot">.</span>
        ));
      }
      other_benefits.push((
        <div key={key}>
          <h4>{data[key].header}</h4>
          <div>
          <p>
            {data[key].content}
            {link}
          </p>
          </div>
        </div>
      ));
    });

    return other_benefits;
  }

  render () {
    const products = GetData("products");
    const products_node = [];
    const product_names = Object.keys(products);
    let product_judge_string = "";

    if (this.props.data["5"]) {
      //This string is used to determine which products to display.
      product_judge_string = "1"+this.props.data["1"]+" 3"+this.props.data["3"]+" 5"+this.props.data["5"];
    } else {
      product_judge_string = "1a 3a 5a";
    }

    console.log(product_judge_string);
    // console.log(product_judge_string);
    product_names.forEach((name,index)=>{
      //if the product's criteria object has a property that matches the judge string, display it.
      if (products[name].criteria[product_judge_string]) {
        const node = (
          <DropDown
            key={index}
            header={name}
            content={this.GenerateDropDownContent(name, product_judge_string)}
            className="strong"
          />
        );
        if (products[name].criteria[product_judge_string] === 1 || products[name].criteria[product_judge_string] > 1) {
          products_node.splice(products[name].criteria[product_judge_string] - 1, 0, node);
        }
        else {
          products_node.push(node);
        }
      }
    });

    return (
      <div id="result-page" className={this.props.shown?"show":""}>
        <h2>Your results</h2>
        <section>
          {products_node}
        </section>
        <Modal
          id="benefit-modal"
          header="All benefits"
          content={this.GenerateModalContent()}
        />
      </div>
    );
  }
}

class FrontPageContent extends React.Component {
  render () {
    return (
      <div id="front-page" className={this.props.shown?"show":""}>
        <div></div>
        <div className="center">
          <div className="g">
            <span className="orbit delay-1 d1" style={{top:"37%",left:"-2%"}}></span>
            <span className="orbit d1" style={{top:"32%",left:"100%"}}></span>
            <span className="orbit delay-4 d1" style={{top:"30%",left:"91%"}}></span>

            <span className="orbit delay-5 s1" style={{top: "4%",left: "35%"}}></span>
            <span className="orbit delay-4 s1" style={{top:"51%",left:"3%"}}></span>
            <span className="orbit delay-5 s1" style={{top:"98%",left:"4%"}}></span>
            <span className="orbit delay-2 s1" style={{top:"103%",left:"40%"}}></span>
            <span className="orbit s1" style={{top:"104%",left:"82%"}}></span>

            <span className="orbit delay-2 s2" style={{top:"85%",left:"95%"}}></span>

            <span className="orbit delay-3 s3" style={{top:"7%",left:"20%"}}></span>
            <span className="orbit delay-1 s3" style={{top:"104%",left:"58%"}}></span>

            <span className="orbit delay-1 c2" style={{top:"19%",left:"7%"}}></span>
            <span className="orbit delay-4 c2" style={{top:"77%",left:"-4%"}}></span>
            <span className="orbit delay-4 c2" style={{top:"101%",left:"19%"}}></span>
            <span className="orbit delay-1 c2" style={{top:"44%",left:"99%"}}></span>
            <span className="orbit delay-3 c2" style={{top:"5%",left:"76%"}}></span>

            <span className="square-float delay-3 s4" style={{top:"30%",left:"42%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"36%",left:"41%"}}></span>
            <span className="square-float delay-4 s4" style={{top:"29%",left:"38%"}}></span>
            <span className="square-float delay-5 s4" style={{top:"38%",left:"37%"}}></span>
            <span className="square-float delay-3 s4" style={{top:"24%",left:"32%"}}></span>
            <span className="square-float delay-4 s4" style={{top:"32%",left:"33%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"41%",left:"32%"}}></span>
            <span className="square-float delay-5 s4" style={{top:"19%",left:"27%"}}></span>
            <span className="square-float delay-2 s4" style={{top:"28%",left:"26%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"37%",left:"27%"}}></span>

            <span className="square-float delay-2 s4" style={{top:"31%",left:"55%"}}></span>
            <span className="square-float delay-4 s4" style={{top:"35%",left:"55%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"28%",left:"59%"}}></span>
            <span className="square-float delay-3 s4" style={{top:"36%",left:"60%"}}></span>
            <span className="square-float delay-2 s4" style={{top:"25%",left:"64%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"31%",left:"63%"}}></span>
            <span className="square-float delay-5 s4" style={{top:"39%",left:"65%"}}></span>
            <span className="square-float delay-3 s4" style={{top:"21%",left:"69%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"28%",left:"70%"}}></span>
            <span className="square-float delay-4 s4" style={{top:"35%",left:"71%"}}></span>
            <span className="square-float delay-1 s4" style={{top:"42%",left:"70%"}}></span>
          </div>
          <div className="z10">
            <figure>
            </figure>
            <h1>Choosing an Azure Data Store</h1>
            <div className="w-660">
              <span className="line-horizontal"></span>
              <p>
                Not sure which data store solutions are right for your unique needs?
                Your search doesn’t have to be daunting. In fact, getting insight on which solutions to investigate further is as simple as making a few selections.
              </p>
              <div>
                <button className="arrow" onClick={(e)=>{this.props.HandlePageChange(1)}}>
                  Ready?
                </button>
              </div>
            </div>
          </div>
        </div>
        <div></div>
      </div>
    );
  }
}

class SidePanelContent extends React.Component {
  render () {
    const aside_classes = [
      "w0",
      "w40",
      "w40",
      "w40",
      "w40",
      "w40",
      "w30 results"
    ];
    return (
      <aside className={aside_classes[this.props.current_page]}>
        <Aside1Content
          shown={this.props.current_page === 1}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
        <Aside2Content
          shown={this.props.current_page === 2}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
        <Aside3Content
          shown={this.props.current_page === 3}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
        <Aside4Content
          shown={this.props.current_page === 4}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
        <Aside5Content
          shown={this.props.current_page === 5}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
        <ResultAsideContent
          shown={this.props.current_page === 6}
          HandlePageChange={this.props.HandlePageChange}
          HandleData={this.props.HandleData}
          data={this.props.data}
        />
      </aside>
    );
  }
}

class PageContent extends React.Component {
  render () {
    const page_classes = [
      "front",
      "carpet",
      "carpet",
      "carpet",
      "carpet",
      "carpet",
      "results"
    ];
    return (
      <section className={page_classes[this.props.current_page] + (this.props.reverse?" reverse":"")}>
        <FrontPageContent shown={this.props.current_page === 0} HandlePageChange={this.props.HandlePageChange} />
        <Page1Content shown={this.props.current_page === 1} />
        <Page2Content shown={this.props.current_page === 2} />
        <Page3Content shown={this.props.current_page === 3} />
        <Page4Content shown={this.props.current_page === 4} />
        <Page5Content shown={this.props.current_page === 5} />
        <ResultPageContent shown={this.props.current_page === 6}
          data={this.props.data}
        />
      </section>
    );
  }
}

class MainHeader extends React.Component {

  HandlePageChange = function (e) {
    this.props.HandlePageChange(e);
  }

  render() {
    return (
      <header>
        <img onClick={(e)=>{this.props.HandlePageChange(0)}} alt="" src={logo_png} />
      </header>
    );
  }
}

class MainFooter extends React.Component {
  render() {
    return (
      <footer>
      </footer>
    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_page: "load",
      data: {
        "3":"c",
        "3-value":50
      }
    };
  }

  CompileStateData = function (data) {
    const old_data_keys = Object.keys(this.state.data);
    const new_data_keys = Object.keys(data);
    let new_data = {};
    old_data_keys.forEach((key,index)=>{
      new_data[key] = this.state.data[key];
    });
    new_data_keys.forEach((key,index)=>{
      new_data[key] = data[key];
    });
    return new_data;
  }

  HandlePageChange = function (page) {
    let reverse = false;
    if (page < this.state.current_page) {
      reverse = true;
    }
    this.setState({ current_page: page, reverse: reverse });
  }

  HandleData = function (data) {
    this.setState({ data:this.CompileStateData(data) });
    console.log(this.CompileStateData(data));
  }

  componentDidMount() {
    requestAnimationFrame(()=>{
      this.HandlePageChange(0);
    });
  }

  render() {
    return (
      <div>
        <MainHeader
          HandlePageChange={(p)=>{this.HandlePageChange(p)}}
        />
        <main>
          <PageContent
            current_page={this.state.current_page}
            reverse={this.state.reverse}
            HandlePageChange={(p)=>{this.HandlePageChange(p)}}
            data={this.state.data}
          />
          <SidePanelContent
            current_page={this.state.current_page}
            HandlePageChange={(p)=>{this.HandlePageChange(p)}}
            HandleData={(d)=>{this.HandleData(d)}}
            data={this.state.data}
          />
        </main>
        <MainFooter />
      </div>
    );
  }
}


ReactDOM.render(<App />, document.getElementById('root'));
