/*
    loadImage

    Handles loading in images asynchronously.
    When an image is loaded, it replaces the image's src attribute from "clear.gif" to the proper image url.
*/

export function LoadImage(url, SuccessCallback, ErrorCallback) {
    var downloadingImage = new Image();
    downloadingImage.onload = function () {
        SuccessCallback(this.src);
    };
    downloadingImage.onerror = function () {
        ErrorCallback();
    };
    downloadingImage.src = url;
}
